package com.cognixia.jump.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognixia.jump.model.ToDo;
import com.cognixia.jump.model.User;
import com.cognixia.jump.repository.ToDoRepository;
import com.cognixia.jump.repository.UserRepository;

import io.swagger.annotations.ApiOperation;

@RequestMapping("/api/todo")
@RestController
public class ToDoController {

	@Autowired
	ToDoRepository repo;
	
	@Autowired
	UserRepository userRepo;
	
	@GetMapping("/{id}")
	@ApiOperation( value = "Get all ToDos created by a certain User.", 
	   notes = "Pass in the User's ID." 
			)
	public ResponseEntity<?> getToDos(@PathVariable int id) {
		List<ToDo> found = repo.getUserToDos(id);
		if(found.isEmpty()) {
			return ResponseEntity.status(404)
					.body("No ToDos found for user with ID = " + id);
		}
		return ResponseEntity.status(200)
				.body(found);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation( value = "Delete a ToDo by its unique ID.", 
	   notes = "Pass in the ToDi's ID." 
			)
	public ResponseEntity<?> deleteToDoById(@PathVariable int id) {
		ToDo deleted = repo.getById(id);
		try {
			repo.deleteById(id);
			return ResponseEntity.status(200)
					.body(deleted);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.status(404).body("ToDo with ID = " + id + " not found.");
		}
	}
	
	
	/* The request body must have the form:
	 {
	    "description": "New Todo for New User, ID 5",
	    "due": "2021-09-02",
	    "user": {
	        "userId": 5
	    	}
	}
	 */
	@PostMapping
	@ApiOperation( value = "Create a new ToDo.", 
	   notes = "Pass in the ToDo's description and due date, as well as a user object containing a userId field with the User's ID."
			)
	public ResponseEntity<?> newToDo(@Valid @RequestBody ToDo toDo) {
		System.out.println(toDo.toString());
		int toDoUserId = toDo.getUser().getUserId();
		toDo.setId(-1);
		System.out.println("User ID = " + toDoUserId);
		Optional<User> found = userRepo.findById(toDoUserId);
		
		if(found.isPresent()) {
			toDo.setUser(found.get());
			ToDo created = repo.save(toDo);
			return ResponseEntity.status(201)
					.body(created);
		}
		else {
			return ResponseEntity.status(400)
					.body("Specified User does not exist");
			
		}
		
	}
	
	// complete a specified ToDo by the ToDo's unique ID
	@PostMapping("/complete/{id}")
	@ApiOperation( value = "Mark a ToDo as completed.", 
	   notes = "Pass in the ToDo's ID."
			)
	public ResponseEntity<?> completeToDoById(@PathVariable int id) {
		
		Optional<ToDo> entry = repo.findById(id);
		
		if(entry.isPresent()) {
			// if the entry is found, update it
			ToDo updated = entry.get();
			updated.setCompleted(true);
			repo.save(updated);
			return ResponseEntity.status(200)
					.body(repo.getById(id));
		}
		// update failed, ToDo not found
		return ResponseEntity.status(404)
				.body("ToDo with ID = " + id + " not found.");
	}
	
	// complete all ToDos by their User's ID
	@PostMapping("/complete/all/{id}")
	@ApiOperation( value = "Complete all ToDos for the specified User.", 
	   notes = "Pass in the User's ID to delete all ToDos created by the User."
			)
	public ResponseEntity<?> completeAllForUser(@PathVariable int id) {
		List<ToDo> updated = repo.findByUserId(id);
		
		if(updated.isEmpty()) {
			// update failed, User not found, or User has no ToDos
			return ResponseEntity.status(404)
					.body("User with ID = " + id + " not found.");
		}
		else {
			for(ToDo t : updated) {
				t.setCompleted(true);
				repo.save(t);
			}
			// update successful
						return ResponseEntity.status(200)
								.body("All ToDos marked complete for User with ID = " + id);
		}
	}
	
	/*
	 * The request body should only contain a date, like "2021-09-08"
	 */
	@PostMapping("/new-date/{id}")
	@ApiOperation( value = "Update the due date of a ToDo.", 
	   notes = "Pass in the ToDo's ID and the new Date, in YYYY-MM-DD format."
			)
	public ResponseEntity<?> newDueDate(@PathVariable int id, @RequestBody LocalDate date) {
		int updated = repo.newDueDate(date, id);
		ToDo entry = repo.getById(id);
		
		if(updated == 1) {
			// update successful
			return ResponseEntity.status(200)
					.body(entry);
		}
		// update failed, ToDo not found
		return ResponseEntity.status(404)
				.body("ToDo with ID = " + id + " not found.");
	}
	
	@DeleteMapping("/delete-all/{id}")
	@ApiOperation( value = "Delete all of a User's ToDos.", 
	   notes = "Pass in the User's ID to delete all their ToDos."
			)
	public ResponseEntity<?> deleteAllForUser(@PathVariable int id) {
		List<ToDo> found = repo.findByUserId(id);
		
		if(found.isEmpty()) {
			// search failed, User not found, or ToDos empty
						return ResponseEntity.status(404)
								.body("User with ID = " + id + " not found, or no ToDos exist.");
		}
		else {
			repo.deleteAll(found);
			return ResponseEntity.status(200)
					.body("All ToDos deleted for User with ID = " + id);
		}
	}
}
