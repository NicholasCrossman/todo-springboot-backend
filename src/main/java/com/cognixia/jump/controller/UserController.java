package com.cognixia.jump.controller;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognixia.jump.exception.EntryNotFoundException;
import com.cognixia.jump.model.User;
import com.cognixia.jump.repository.UserRepository;

import io.swagger.annotations.ApiOperation;

@RequestMapping("/api/user")
@RestController
public class UserController {
	@Autowired
	UserRepository repo;
	
	@PostMapping
	@ApiOperation( value = "Get a User object by its username and password.", 
	   notes = "Provide the User's username and password." 
			)
	public ResponseEntity<?> getUserByInfo(@RequestBody Map<String, String> loginInfo) {
		String userName = loginInfo.get("userName");
		String password = loginInfo.get("password");
		User loggedIn = repo.findByUserNameAndPassword(userName, password);
		
		if(loggedIn.getUserId() == -1) {
			// login failed
			return ResponseEntity.status(404)
					.body("Login unsuccessful for user = " + userName);
		}
		// login succeeded
		return ResponseEntity.status(200)
				.body(loggedIn);
	}
	
	@PostMapping("/update/{id}")
	@ApiOperation( value = "Update a User's username and password.", 
	   notes = "Pass in the User's ID, along with the updated username and password."
			)
	public ResponseEntity<?> updateUserInfo(@PathVariable int id, @RequestBody Map<String, String> newInfo) 
			throws EntryNotFoundException {
		String userName = newInfo.get("userName");
		String password = newInfo.get("password");
		
		Optional<User> user = repo.findById(id);
		
		if(user.isEmpty()) {
			throw new EntryNotFoundException("Exception: Update failed. User not found with ID = " + id);
		}
		else {
			User u = user.get();
			u.setUserName(userName);
			u.setPassword(password);
			
			repo.save(u);
			
			return ResponseEntity.status(200)
					.body(repo.getById(id));
		}
	}
	
	@PostMapping("/new")
	@ApiOperation( value = "Create a new User.",
	   notes = "Provide a unique Username and a password."
			)
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
		user.setUserId(-1);
		User created = repo.save(user);
		
		return ResponseEntity.status(201)
				.body(created);
	}

}
