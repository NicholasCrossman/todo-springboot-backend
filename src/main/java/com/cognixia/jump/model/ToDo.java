package com.cognixia.jump.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

//import com.cognixia.jump.model.User;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class ToDo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "The id the database will generate using auto incrementing for the ToDo.")
	private Integer id;
	
	@NotBlank
	@ApiModelProperty(notes = "The description of the ToDo.")
	private String description;
	
	@ApiModelProperty(notes = "A flag to indicate if the ToDo has been completed.")
	private boolean isCompleted;
	
	@NotNull
	@ApiModelProperty(notes = "The due date of the ToDo. This is only the day, without any time information.")
	private LocalDate due;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "userId", nullable = true)
	//@JsonIgnore
	private User user;
	
	public ToDo() {
		this(-1, "N/A", LocalDate.now(), new User());
	}

	public ToDo(Integer id, @NotBlank String description, @NotBlank LocalDate due, @NotNull User user) {
		super();
		this.id = id;
		this.description = description;
		this.isCompleted = false;
		this.due = due;
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public LocalDate getDue() {
		return due;
	}

	public void setDue(LocalDate due) {
		this.due = due;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ToDo [id=" + id + ", description=" + description + ", isCompleted=" + isCompleted + ", due=" + due
				+ "]";
	}
	
	

}
