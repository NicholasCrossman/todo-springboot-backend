package com.cognixia.jump.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

//import com.cognixia.jump.model.ToDo;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "The id the database will generate using auto incrementing for the User.")
	private Integer userId;
	
	@NotBlank
	@ApiModelProperty(notes = "The User's username. This MUST BE UNIQUE.")
	private String userName;
	
	@NotBlank
	@ApiModelProperty(notes = "The User's password.")
	private String password;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ToDo> toDos;

	public User() {
		this(-1, "N/A", "N/A");
	}
	
	public User(Integer id, String userName, String password) {
		super();
		this.userId = id;
		this.userName = userName;
		this.password = password;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer id) {
		this.userId = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

//	public List<ToDo> getToDos() {
//		return toDos;
//	}

	public void setToDos(List<ToDo> toDos) {
		this.toDos = toDos;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + userId + ", userName=" + userName + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(password, userId, userName);
	}

	public boolean equals(User obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(password, other.password) && Objects.equals(userId, other.userId)
				&& Objects.equals(userName, other.userName);
	}
	
	

}
