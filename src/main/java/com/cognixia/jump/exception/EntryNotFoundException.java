package com.cognixia.jump.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntryNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public EntryNotFoundException(String message) {
		// calls the Exception constructor
		super(message);
	}

}
