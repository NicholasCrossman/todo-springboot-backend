package com.cognixia.jump.repository;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cognixia.jump.model.ToDo;

@Repository
public interface ToDoRepository extends JpaRepository<ToDo, Integer> {
	
	@Query("select t from ToDo t where t.user.userId = ?1")
	List<ToDo> getUserToDos(int id);
	
	@Query("select t from ToDo t where t.user.userId = ?1")
	List<ToDo> findByUserId(int userId);
	
	@Query("update ToDo t set t.due = ?1 where t.id = ?2")
	@Transactional
	@Modifying
	int newDueDate(LocalDate d, int id);
	
	@Transactional
	@Modifying
	int deleteByUserUserId(int userId);

}
